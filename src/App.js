import React from 'react';
import './App.css';  
import 'bootstrap/dist/css/bootstrap.min.css'
import { Container, Row, Col, Form, Button, Alert, Breadcrumb, Card } from 'react-bootstrap'

function App() {
  return (
    <div className="App">
    <Container> 
      <Row>
        <Col>
          <Card className="mb-3"  border="light">
              <Card.Body>
                <Card.Title>
                  Atlantis Systems - challenge
                </Card.Title>
              </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form> 
            <Form.Group controlId="exampleForm.ControlTextarea1">
              <Form.Label>Pegar el texto aqui</Form.Label>
              <Form.Control as="textarea" rows={3} />
            </Form.Group>
          </Form> 
        </Col> 
      </Row>
    </Container>
    </div>
  );
} 

export default App;
